﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BancoCentralNicaragua
{
    public class Consultar
    {
        public static double GetDia(int ano, int mes, int dia)
        {
            try
            {
                var web = new bcnService.Tipo_Cambio_BCNSoapClient();
                double cambio = web.RecuperaTC_Dia(ano, mes, dia);
                return cambio;
            }
            catch (Exception)
            {
                return 0.0;
            }
        }

        public static double GetDia(DateTime fecha)
        {
            try
            {
                var web = new bcnService.Tipo_Cambio_BCNSoapClient();
                double cambio = web.RecuperaTC_Dia(fecha.Year, fecha.Month, fecha.Day);
                return cambio;
            }
            catch (Exception)
            {
                return 0.0;
            }
        }
    }
}
